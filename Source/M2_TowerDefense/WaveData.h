// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Enemy.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS()
class M2_TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()

public: 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<class AEnemy>> EnemyTypes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 totalNumOfSpawns = 4; 

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 numOfEnemyKilled;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 numOfEnemyReachedCore;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float delayBetSpawn = 2; //delay between spawns(enemy)

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rewards")
	int32 killReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rewards")
	int32 clearReward;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float enemyHP;

	//UFUNCTION(BlueprintPure, BlueprintCallable)
	//int32 waveReward();

	UFUNCTION(BlueprintCallable)
	int32 waveProgress();

	UFUNCTION()
	int32 getEnemyKillCount(int32 enemyDeadCount);

	UFUNCTION()
	int32 getEnemReachedCoreCount(int32 enemyAliveCount);

	

};
