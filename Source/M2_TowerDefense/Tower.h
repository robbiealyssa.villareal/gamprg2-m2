// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy.h"
#include "Tower.generated.h"

UCLASS()
class M2_TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* baseTurretMesh;

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* turretMesh;

	UPROPERTY(EditAnywhere)
	class USphereComponent* SphereRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UArrowComponent* turretArrow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UArrowComponent* BarrelArrow;



	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Enemy in Range")
	TArray<AEnemy*> EnemyInRange; //AActor

	UPROPERTY(EditAnywhere, Category = "Projectile")
	TSubclassOf<class AProjectile> projectileClass;

	//Stats
	UPROPERTY(EditAnywhere, Category = "Turret Stats")
	float projectileSpeed = 2000.f;
	UPROPERTY(EditAnywhere, Category = "Turret Stats")
	float rotSpeed = 1.0f;
	UPROPERTY(EditAnywhere, Category = "Turret Stats")
	float fireRate = 1.f;
	UPROPERTY(EditAnywhere, Category = "Turret Stats")
	float range = 600.f; //1000
	UPROPERTY(EditAnywhere, Category = "Turret Stats")
	float damage = 5.f;

	//

	UPROPERTY(BlueprintReadOnly)
	class AEnemy* target; //AActor

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void LookAtTarget();

	UFUNCTION()
	void FollowTarget();

	UFUNCTION()
	void Fire();

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool enemyFound;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool isFiring;
	

private: 
	float fireRateTime = 0;

};
