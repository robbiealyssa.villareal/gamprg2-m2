// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "HealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Enemy.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));
	bullet->SetupAttachment(RootComponent);
	bullet->bEditableWhenInherited = true;

	bulletSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Bullet Sphere"));
	bulletSphere->SetupAttachment(bullet);
	bulletSphere->bEditableWhenInherited = true;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	AddOwnedComponent(ProjectileMovement);
	ProjectileMovement->bEditableWhenInherited;

}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(5.f);

	bulletSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::BeginOverlap);

}

void AProjectile::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		
		//enemy->health->applyDamageHealth(damage);
		enemy->TakeDamage(damage);

		this->Destroy();
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("Projectile hit target"));
	}
	
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

//void AProjectile::applyDamage(float dmg)
//{
	//damage = dmg;
//}

