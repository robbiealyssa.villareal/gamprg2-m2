// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class M2_TOWERDEFENSE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	

	

	

public:	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxHealth; // change to 5 for add buff every wave 

	UFUNCTION(BlueprintCallable)
	int setHealth(float addHp);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float getHealth();

	UFUNCTION(BlueprintCallable)
	void applyDamageHealth(float dmg);

	//UFUNCTION(BlueprintCallable, BlueprintPure)
	bool isDead;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float currentHealth;

	// void AddMaxHealth?
		
};
