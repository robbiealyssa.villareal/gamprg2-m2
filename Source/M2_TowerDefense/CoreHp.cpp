// Fill out your copyright notice in the Description page of Project Settings.


#include "CoreHp.h"
#include "Enemy.h"
#include "M2GameMode.h"


// Sets default values
ACoreHp::ACoreHp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(StaticMesh);

	enemyOnCore = false;

	//StaticMesh->OnComponentHit.AddDynamic(this, &ACoreHp::OnHit); // try begin overlap 
}

// Called when the game starts or when spawned
void ACoreHp::BeginPlay()
{
	Super::BeginPlay();
	
	StaticMesh->OnComponentHit.AddDynamic(this, &ACoreHp::OnHit);
}

void ACoreHp::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Enemy Reached Core"));
		
		OnGetHp(this);
		//enemy->OnReachedCore.AddDynamic(this, &ACoreHp::OnEnemyReachedCore);
		enemy->Destroy();
	}
}

void ACoreHp::ReduceHp()
{
	health -= 1.;
	//GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Red, TEXT("-HP"));

	if (health <= 0)
	{
		health = 0;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Game over"));
		//stop spawning 
	}
}

void ACoreHp::OnEnemyReachedCore()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Enemy Reached Core"));
	
}

// Called every frame
void ACoreHp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

