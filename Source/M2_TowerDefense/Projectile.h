// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Projectile.generated.h"

UCLASS()
class M2_TOWERDEFENSE_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	UPROPERTY(VisibleAnywhere)
	class UProjectileMovementComponent* ProjectileMovement;



	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* bullet;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USphereComponent* bulletSphere;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	//class UArrowComponent* bulletArrow;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float damage = 5.f;


	//

	UFUNCTION(BlueprintImplementableEvent)
	void OnTrigger();

	UFUNCTION(BlueprintImplementableEvent)
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			FVector NormalImpulse, const FHitResult& Hit);

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION(BlueprintImplementableEvent)
	//void applyDamage(float dmg);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float projSpeed = 5000.f; // 5

};
