// Fill out your copyright notice in the Description page of Project Settings.


#include "WaveData.h"

int32 UWaveData::waveProgress()
{	//enemy killed + reached core / total num of enemies 
	return totalNumOfSpawns;
}

int32 UWaveData::getEnemyKillCount(int32 enemyDeadCount)
{
	numOfEnemyKilled = enemyDeadCount;
	return numOfEnemyKilled;
}

int32 UWaveData::getEnemReachedCoreCount(int32 enemyAliveCount)
{
	numOfEnemyReachedCore = enemyAliveCount;
	return numOfEnemyReachedCore;
}
