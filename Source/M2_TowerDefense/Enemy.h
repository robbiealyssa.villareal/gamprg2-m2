// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HealthComponent.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath); //add param for reward
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReachedCore);
//reached core

UCLASS()
class M2_TOWERDEFENSE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();
	
	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDeath;

	UPROPERTY(BlueprintAssignable)
	FOnReachedCore OnReachedCore;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool reachedCore;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	//lsit of waypoints 
	UPROPERTY(EditAnywhere)
	int32 currentWaypointPos;

	UPROPERTY(EditAnywhere, Category = "Waypoints")
	TArray<AActor*> Waypoints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UWaveData* WaveData;

	UFUNCTION()
	void MoveToWaypoint();

	UFUNCTION(BlueprintCallable)
	void OnEnemyReachedCore();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UHealthComponent* health;

	

	UFUNCTION(BlueprintCallable)
	void TakeDamage(float dmg);

	UFUNCTION(BlueprintCallable)
	void Die();

private: 

	UPROPERTY(EditAnywhere)
	int32 speed = 1;

	

};
