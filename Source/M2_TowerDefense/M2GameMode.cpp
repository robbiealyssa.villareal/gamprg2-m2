// Fill out your copyright notice in the Description page of Project Settings.


#include "M2GameMode.h"
#include "Enemy.h"
#include "Spawner.h"
#include "PlayerGold.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"


void AM2GameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (maxWaves <= currentWave)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Black, TEXT("Game Over"));
	}

}

int AM2GameMode::addGold(float amount)
{
	gold += amount;
	return gold;
}

int32 AM2GameMode::getCurrentWave(int32 waveNum)
{
	currentWave = waveNum;
	return currentWave;
}

int32 AM2GameMode::getNumOfSpawns(int32 enemSpawned)
{
	numOfSpawns = enemSpawned;
	return numOfSpawns;
}

void AM2GameMode::addEnemyKillCount(float amount) 
{
	enemyKillCount += 1; 
	addGold(amount);
}

void AM2GameMode::addEnemReachedCoreCount()  
{
	enemyReachedCoreCount += 1; 
}
