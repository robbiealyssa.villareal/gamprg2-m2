// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoreHp.generated.h"

UCLASS()
class M2_TOWERDEFENSE_API ACoreHp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoreHp();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool enemyOnCore;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float health = 100.;

	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* StaticMesh;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(BlueprintImplementableEvent)
	void OnGetHp(class ACoreHp* core);

	UFUNCTION(BlueprintCallable)
	void ReduceHp();

	UFUNCTION(BlueprintCallable)
	void OnEnemyReachedCore();



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
