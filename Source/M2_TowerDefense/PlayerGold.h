// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerGold.generated.h"

UCLASS()
class M2_TOWERDEFENSE_API APlayerGold : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerGold();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite)
	float gold;

	//UFUNCTION()
	//void addGold(float amount);
	//UFUNCTION()
	//void deductGold(float amount);

};
