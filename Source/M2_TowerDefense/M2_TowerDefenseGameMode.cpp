// Copyright Epic Games, Inc. All Rights Reserved.

#include "M2_TowerDefenseGameMode.h"
#include "M2_TowerDefenseCharacter.h"
#include "UObject/ConstructorHelpers.h"

AM2_TowerDefenseGameMode::AM2_TowerDefenseGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
