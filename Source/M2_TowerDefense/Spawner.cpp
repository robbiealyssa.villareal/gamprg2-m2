// Fill out your copyright notice in the Description page of Project Settings.

#include "M2GameMode.h"
#include "TimerManager.h"
#include "Enemy.h"
#include "EnemyAIController.h"
#include "HealthComponent.h"
#include "WaveData.h"
#include "Waypoint.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Spawner.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	SpawnPointEnemy = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnPoint"));
	SpawnPointEnemy->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	
	// Set Timer for start wave count and Enemy spawner
	GetWorldTimerManager().SetTimer(timerWave, this, &ASpawner::PrepareNextWave, 6, false);

	GetWorldTimerManager().SetTimer(timer, this, &ASpawner::SpawnEnemy, 10, false); //get spawnDelay wave(GameMode)

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), Waypoints);
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AM2GameMode* gameMode = Cast<AM2GameMode>(GetWorld()->GetAuthGameMode<AM2GameMode>());

	// If all enemy spawned (per wave)
	if (spawnedEnemyCount > WaveData[currentWaveCount]->totalNumOfSpawns)
	{
		GetWorldTimerManager().ClearTimer(timer);		

		GetWorldTimerManager().SetTimer(timer, this, &ASpawner::SpawnEnemy, 15, false); //15
		
	}

	if (spawnedEnemyCount == WaveData[currentWaveCount]->totalNumOfSpawns)
	{
		GetWorldTimerManager().SetTimer(timerWave, this, &ASpawner::PrepareNextWave, 10, false);
		
	}

	// If all waves finish
	if (gameMode->maxWaves <= currentWaveCount)
	{
		GetWorldTimerManager().ClearTimer(timer);
		GetWorldTimerManager().ClearTimer(timerWave);

		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Black, TEXT("Game Over"));
	}

}

void ASpawner::SpawnEnemy()
{
	UWorld* const World = GetWorld();

	if (World)
	{

		AM2GameMode* gameMode = Cast<AM2GameMode>(GetWorld()->GetAuthGameMode<AM2GameMode>());

		FVector SpawnLoc = this->GetActorLocation();

		EnemyIndex = UKismetMathLibrary::RandomIntegerInRange(0, WaveData[currentWaveCount]->EnemyTypes.Num() - 1);

		AEnemy* spawnEnemy = World->SpawnActor<AEnemy>(WaveData[currentWaveCount]->EnemyTypes[EnemyIndex], FTransform(SpawnLoc));
		
		//
		spawnEnemy->health->MaxHealth = WaveData[currentWaveCount]->enemyHP;
		spawnEnemy->health->currentHealth = spawnEnemy->health->MaxHealth;
		//spawnEnemy->health->setHealth(WaveData[currentWaveCount]->enemyHP);
		//

		spawnEnemy->currentWaypointPos = waypointPos;
		spawnEnemy->MoveToWaypoint();

		//delay 
		spawnDelay = FMath::FRandRange(minDelay, maxDelay);
		GetWorldTimerManager().SetTimer(timer, this, &ASpawner::SpawnEnemy, spawnDelay, false);// delay between spawns (Wavedata)

		//add spawnEnemyCount
		spawnedEnemyCount++;
		
		//get num of enemies spawned 
		gameMode->getNumOfSpawns(spawnedEnemyCount);
		

		//spawnEnemy->OnDeath.AddDynmic(gameMode, &AM2GameMode::AddEnemyDestroy);

		spawnEnemy->OnReachedCore.AddDynamic(gameMode, &AM2GameMode::addEnemReachedCoreCount);
	}

}

void ASpawner::PrepareNextWave()
{   
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, TEXT("Preparing next wave"));
	AM2GameMode* gameMode = Cast<AM2GameMode>(GetWorld()->GetAuthGameMode<AM2GameMode>());

	// Reward buffs add gold:
	// kill reward -> +5
	gameMode->addEnemyKillCount(WaveData[currentWaveCount]->killReward);
	// clear wave reward -> +10
	gameMode->addGold(WaveData[currentWaveCount]->clearReward);

	// Add wave number before start spawning
	currentWaveCount++;
	WaveData[currentWaveCount];
	WaveDataCurrent = WaveData[currentWaveCount];
	gameMode->getCurrentWave(currentWaveCount);
	


	spawnedEnemyCount = 0;
	
	enemyReachedCore = 0;

	enemyKilled = 0;

	
}

int32 ASpawner::getEnemyKilledCount(int32 enemyDeadCount)//delete
{
	enemyKilled = enemyDeadCount;
	return enemyKilled;
}

int32 ASpawner::getEnemReachedCoreCount(int32 enemyAliveCount)//delete
{
	enemyReachedCore = enemyAliveCount;
	return enemyReachedCore;
}

