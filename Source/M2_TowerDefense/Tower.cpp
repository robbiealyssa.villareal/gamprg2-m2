// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Components/SphereComponent.h"
#include "Components/ActorComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Enemy.h"
#include "Projectile.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Scene")));

	baseTurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Turret Mesh"));
	baseTurretMesh->SetupAttachment(RootComponent);
	baseTurretMesh->bEditableWhenInherited = true;

	turretArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Turret Arrow"));
	turretArrow->SetupAttachment(baseTurretMesh);
	turretArrow->bEditableWhenInherited = true;

	turretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Mesh"));
	turretMesh->SetupAttachment(turretArrow);
	turretMesh->bEditableWhenInherited = true;

	BarrelArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Barrel Arrow"));
	BarrelArrow->SetupAttachment(turretArrow);
	BarrelArrow->bEditableWhenInherited = true;

	SphereRange = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	SphereRange->SetupAttachment(RootComponent);
	SphereRange->SetSphereRadius(range);
	SphereRange->bEditableWhenInherited = true;


	enemyFound = false;
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();

	//isFiring = false;

	SphereRange->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlapBegin);
	SphereRange->OnComponentEndOverlap.AddDynamic(this, &ATower::OnOverlapEnd);
	
}

void ATower::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{

		enemyFound = true;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Enemy Detected"));

		//Add to array
		EnemyInRange.Add(enemy);

		//set target
		target = EnemyInRange[0];
	}
}

void ATower::LookAtTarget()
{
	if (EnemyInRange.Num() > 0) 
	{

		if (EnemyInRange[0] == nullptr) return;

		target = Cast<AEnemy>(EnemyInRange[0]);


		UKismetSystemLibrary::DrawDebugLine(GetWorld(), BarrelArrow->GetComponentLocation(), EnemyInRange[0]->GetTargetLocation(),
			FColor::Red, 0.5f, 2.0f);

	}
}

void ATower::FollowTarget()
{	

	FRotator rot = UKismetMathLibrary::FindLookAtRotation(target->GetActorLocation(), BarrelArrow->GetComponentLocation());
	
	turretMesh->SetWorldRotation(rot); 

}

void ATower::Fire()
{

	AProjectile* spawnProjectile = GetWorld()->SpawnActor<AProjectile>(projectileClass, BarrelArrow->GetComponentTransform());
	
}

void ATower::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{ 
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		//Remove current enemy
		EnemyInRange.Remove(enemy);

		if (EnemyInRange.Num() > 0)
		{
			//target = EnemyInRange[0];
			LookAtTarget();
		}
		else {
			target = nullptr;
		}
		
	}
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if(target != nullptr)
	{
		//FVector targetLoc = target->GetActorLocation();
		FVector targetLoc = EnemyInRange[0]->GetActorLocation();
		FVector turretLoc = turretMesh->GetComponentLocation();
		FVector vectorDir = targetLoc - turretLoc;
		FRotator dirRot = UKismetMathLibrary::Conv_VectorToRotator(vectorDir);
		//turretMesh->SetWorldRotation(dirRot);

		FRotator offSet = FRotator(0, 90, 0);
		dirRot += offSet;
		turretMesh->SetWorldRotation(dirRot);

		if (fireRateTime <= 0)
		{
			Fire();
			
			fireRateTime = 1. / fireRate;
		}
		else {
			fireRateTime -= DeltaTime;
		}

	}


	if(!target) 
	{
		LookAtTarget();
		
	}
	else {
		
		FollowTarget();
		

	}


}

