// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Enemy.h"
#include "EnemyAIController.h"
#include "Waypoint.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Spawner.generated.h"


UCLASS()
class M2_TOWERDEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 currentWaveCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 spawnedEnemyCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 enemyReachedCore;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 enemyKilled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 addHPBuff = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 addGoldBuff;

	UFUNCTION()
	void SpawnEnemy();

	UFUNCTION()
	void PrepareNextWave();


	//UFUNCTION()
	//void stopSpawn(); //Stop all timers 

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* SpawnPointEnemy;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 EnemyIndex;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UWaveData* WaveDataCurrent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<class UWaveData*> WaveData;

	/*UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 currentWave;*/
	//UPROPERTY(EditAnywhere)
	//TArray<WaveData*> waveData


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float minDelay = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float maxDelay = 2.5f;

	FTimerHandle timer; //private

	FTimerHandle timerWave;

	FTimerHandle timerAddWave;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	int32 waypointPos;

	UPROPERTY(EditAnywhere, Category = "Waypoints")
	TArray<AActor*> Waypoints;



	UFUNCTION() 
	int32 getEnemyKilledCount(int32 enemyDeadCount);//delete

	UFUNCTION() 
	int32 getEnemReachedCoreCount(int32 enemyAliveCount);//delete

	
	

private:

	float spawnDelay;

	
};
