// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	MaxHealth = 5;
	
	currentHealth = MaxHealth;
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	isDead = false;
}

int UHealthComponent::setHealth(float addHp)
{
	MaxHealth = MaxHealth + addHp;
	MaxHealth = currentHealth;
	return MaxHealth;
}

float UHealthComponent::getHealth()
{
	return 0.0f;
}

void UHealthComponent::applyDamageHealth(float dmg)
{
	currentHealth -= dmg;
	if (currentHealth <= 0)
	{
		currentHealth = 0;
		isDead = true;
	}
}




