// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "Waypoint.h"
#include "M2GameMode.h"
#include "EnemyAIController.h"
#include "Kismet/GameplayStatics.h"
#include "WaveData.h"
#include "HealthComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

	reachedCore = false;
	
	OnDeath.AddDynamic(this, &AEnemy::Die);
	OnReachedCore.AddDynamic(this, &AEnemy::OnEnemyReachedCore);
	
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), Waypoints);
	MoveToWaypoint();

	//OnDestroyed.AddDynamic(this, &AEnemy::Die);

}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (health->currentHealth <= 0)
	{
		health->currentHealth = 0;
		this->Destroy();
	}

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::MoveToWaypoint()
{
	AEnemyAIController* AIController = Cast<AEnemyAIController>(GetController());

	if (AIController)
	{
		if (currentWaypointPos <= Waypoints.Num())
		{
			for (AActor* waypoint : Waypoints)
			{
				AWaypoint* wp = Cast<AWaypoint>(waypoint);

				//if(wp){}
				if (wp->GetWaypointPos() == currentWaypointPos)
				{
					AIController->MoveToActor(wp, speed, false);
					currentWaypointPos++;
					break;
				}
				else if (wp == nullptr)
				{
					OnReachedCore.Broadcast();
					this->Destroy();
					return;
				}

			}
		} 
		
	}
}

void AEnemy::OnEnemyReachedCore()   
{
	OnReachedCore.Broadcast();
	//reachedCore = true;
}

void AEnemy::TakeDamage(float dmg)
{
	if (health)
	{
		health->applyDamageHealth(dmg);

		if (health->currentHealth <= 0)
		{
			//Die();
			OnDeath.Broadcast();
			//// broadcast with gold reward
			//this->Destroy();
		}
	}
}

void AEnemy::Die()
{
	
	if (health->currentHealth <= 0)
	{
		AM2GameMode* gameMode = Cast<AM2GameMode>(GetWorld()->GetAuthGameMode<AM2GameMode>());
		gameMode->enemyKillCount;
		gameMode->addGold(5);
		
		// broadcast with gold reward
		this->Destroy();
	}

	
}

