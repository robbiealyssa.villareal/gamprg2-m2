// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "M2GameMode.generated.h"

/**
 * 
 */
UCLASS()
class M2_TOWERDEFENSE_API AM2GameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 currentWave;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 numOfSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float delayBetWaves = 5;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 maxWaves = 9; //10

	//UPROPERTY(EditAnywhere)
	//TArray<WaveData*> wave

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 enemyKillCount; 

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 enemyReachedCoreCount; 

	UPROPERTY(BlueprintReadOnly)
	float gold = 10;

	UFUNCTION(BlueprintCallable)
	int addGold(float amount);


	UFUNCTION()
	int32 getCurrentWave(int32 waveNum);

	UFUNCTION()
	int32 getNumOfSpawns(int32 enemSpawned);

	UFUNCTION() 
	void addEnemyKillCount(float amount);

	UFUNCTION() 
	void addEnemReachedCoreCount();
};
