// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerGold.h"

// Sets default values
APlayerGold::APlayerGold()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerGold::BeginPlay()
{
	Super::BeginPlay();
	
	gold = 10;
}

// Called every frame
void APlayerGold::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

