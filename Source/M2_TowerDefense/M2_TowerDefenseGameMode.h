// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "M2_TowerDefenseGameMode.generated.h"

UCLASS(minimalapi)
class AM2_TowerDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AM2_TowerDefenseGameMode();
};



